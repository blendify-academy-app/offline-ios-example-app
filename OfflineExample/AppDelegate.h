//
//  AppDelegate.h
//  OfflineExample
//
//  Created by Lien Van Nuffel on 29/08/16.
//  Copyright © 2016 USW. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CouchbaseLite/CouchbaseLite.h>
#import <CouchbaseLiteListener/CBLListener.h>
#import "CBLRegisterJSViewCompiler.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

