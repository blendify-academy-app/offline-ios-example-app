//
//  AppDelegate.m
//  OfflineExample
//
//  Created by Lien Van Nuffel on 29/08/16.
//  Copyright © 2016 USW. All rights reserved.
//

#import "AppDelegate.h"

// #define LISTENER 1

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // Run the DB.
    CBLManager* dbmgr = [CBLManager sharedInstance];
    CBLRegisterJSViewCompiler();
    
    [dbmgr.customHTTPHeaders setObject:@"*" forKey:@"Access-Control-Allow-Origin"];
    [dbmgr.customHTTPHeaders setObject:@"GET, PUT, POST, DELETE" forKey: @"Access-Control-Allow-Methods"];
    [dbmgr.customHTTPHeaders setObject:@"content-type, accept" forKey: @"Access-Control-Allow-Headers"];

#ifdef LISTENER
    // Run the listener.
    CBLListener* listener = [[CBLListener alloc] initWithManager: dbmgr port: 5800];
    
    [listener setBonjourName: @"TestSrv" type: @"_cbl._tcp."];
    
    listener.requiresAuth = NO;
    listener.readOnly = NO;
    
    NSError* error;
    BOOL status = [listener start:&error];
    
    if (!status) {
        NSLog(@"Error: %@", error.localizedDescription);
        return NO;
    }
    
    NSLog(@"Couchbase port = %d", listener.port);
#endif
    
    NSLog(@"Internal URL = %@", dbmgr.internalURL);
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
