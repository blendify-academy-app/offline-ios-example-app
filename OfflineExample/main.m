//
//  main.m
//  OfflineExample
//
//  Created by Lien Van Nuffel on 29/08/16.
//  Copyright © 2016 USW. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
