//
//  VideoPlayerController.m
//  OfflineExample
//
//  Created by Lien Van Nuffel on 21/09/16.
//  Copyright © 2016 USW. All rights reserved.
//

#import "VideoPlayerController.h"

@interface VideoPlayerController()

@property NSURL *url;

@end

@implementation VideoPlayerController

- (id) initWithURL:(NSURL *)videoUrl nibName:(NSString *)name nibBundle:(NSBundle *)bundle {
    self = [super initWithNibName:name bundle:bundle];
    
    if (self) {
        self.url = videoUrl;
    }
    
    return self;
}

- (void) viewDidLoad {
    [super viewDidLoad];
    
    AVPlayer *player = [AVPlayer playerWithURL:self.url];
    self.player = player;
    
    [self.player play];
}

@end