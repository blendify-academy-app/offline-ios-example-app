//
//  VideoHandler.m
//  OfflineExample
//
//  Created by Lien Van Nuffel on 21/09/16.
//  Copyright © 2016 USW. All rights reserved.
//

#import "FileDownloader.h"

@implementation FileDownloader

@synthesize delegate;

- (bool) downloadFile:(NSString *)downloadUrl fileKey:(NSString *)key fileExt:(NSString *)ext
{
    self.fileManager = [[NSFileManager alloc] init];
    self.key = key;
    self.ext = ext;
    self.fileName = [NSString stringWithFormat:@"%@.%@", key, ext];
    
    NSURL *destinationUrl = [self getDestinationUrl];
    
    bool exists = [self.fileManager fileExistsAtPath:[destinationUrl path]];

    if (!exists) {
        NSLog(@"Attempting to download %@", self.fileName);
        
        NSURL *url =[NSURL URLWithString:downloadUrl];
        NSURLRequest *downloadRequest = [NSURLRequest requestWithURL:url];
    
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *urlSession = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:nil];
    
        NSURLSessionDownloadTask *downloadTask = [urlSession downloadTaskWithRequest:downloadRequest];
        [downloadTask resume];
        
        return true;
    } else {
        [delegate fileReady:self isThreaded:FALSE];
    }
    
    return false;
}

- (NSURL *) getDestinationUrl
{
    NSArray *documentURLS = [self.fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    NSURL *documentsDirectory = [documentURLS firstObject];
    
    NSURL *destinationUrl = [documentsDirectory URLByAppendingPathComponent:self.fileName];
    
    return destinationUrl;
}

- (void) URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    NSURL *destinationUrl = [self getDestinationUrl];
    
    NSData *fileData = [NSData dataWithContentsOfURL:location];
    [fileData writeToURL:destinationUrl atomically:NO];
    
    NSLog(@"Downloaded %@ and saved to %@", self.fileName, [destinationUrl absoluteString]);
    
    _storedUrl = destinationUrl;
}

- (void) URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    if (error == nil) {
        [delegate fileReady:self isThreaded:TRUE];
    } else {
        [[NSFileManager defaultManager] removeItemAtURL:_storedUrl error:nil];
        
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Downloading" message:@"We tried to download a file that is no longer accessible. Try to resync the class." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [av show];
    }
}

- (void) URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    float progress = (float) totalBytesWritten / (float) totalBytesExpectedToWrite;
    self.progress = (int) (progress * 100);
    
    [delegate progressChanged:self];
}

- (int) getProgress
{
    return self.progress;
}

@end