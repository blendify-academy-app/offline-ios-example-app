//
//  VideoHandler.h
//  OfflineExample
//
//  Created by Lien Van Nuffel on 21/09/16.
//  Copyright © 2016 USW. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DownloadDelegate.h"

@interface FileDownloader:NSObject <NSURLSessionDelegate, NSURLSessionDownloadDelegate> {
    NSURL* _storedUrl;
}

@property (strong, nonatomic) NSString *key;
@property (strong, nonatomic) NSString *ext;
@property (strong, nonatomic) NSString *fileName;
@property (strong, nonatomic) NSFileManager *fileManager;

@property (retain, nonatomic) id <DownloadDelegate> delegate;

@property int progress;

- (bool) downloadFile:(NSString *)downloadUrl fileKey:(NSString *)key fileExt:(NSString *)ext;

- (void) setDelegate:(id <DownloadDelegate>)incomingDelegate;

- (NSURL *) getDestinationUrl;

- (int) getProgress;

@end