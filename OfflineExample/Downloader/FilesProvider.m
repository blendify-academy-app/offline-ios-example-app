//
//  FilesProvider.m
//  OfflineExample
//
//  Created by Lien Van Nuffel on 21/09/16.
//  Copyright © 2016 USW. All rights reserved.
//

#import "FilesProvider.h"
#import <MediaPlayer/MediaPlayer.h>

@implementation FilesProvider

- (id) init:(ViewController *)viewController
{
    self = [super init];
    
    self.downloads = [[NSMutableArray alloc] initWithCapacity:50];
    self.viewController = viewController;
    
    return self;
}

- (void) bindEvents {
    [Jockey on:@"ios-download-file" perform:^(NSDictionary *payload) {
        NSString* url = payload[@"url"];
        NSString* key = payload[@"key"];
        NSString* ext = payload[@"ext"];
        
        [self downloadFileInQueue:url fileKey:key fileExt:ext];
    }];
    
    [Jockey on:@"ios-trigger-action" perform:^(NSDictionary *payload) {
        NSString* key = payload[@"key"];
        
        [self triggerFileAction:key];
    }];
}

- (void) downloadFileInQueue:(NSString *)url fileKey:(NSString *)key fileExt:(NSString *)ext
{
    FileDownloader *fileDownloader = [[FileDownloader alloc] init];
    fileDownloader.delegate = self;
    
    bool downloading = [fileDownloader downloadFile:url fileKey:key fileExt:ext];
    
    if (downloading) {
        NSLog(@"Downloading %@", url);
        [self.downloads addObject:fileDownloader];
    }
}

- (void) progressChanged:(FileDownloader *)sender
{
    int count = 0;
    int sum = 0;
    int current = 0;
    int total = 0;
    
    for (FileDownloader *fileDownloader in self.downloads) {
        current = [fileDownloader getProgress];
        sum += current;
        total++;
        if (current != 100) {
            count++;
        }
    }

    int percent = (int) ((float) sum / (float) total);
    
    // NSLog(@"Percent: %i", percent);
    
    NSString *progress = [NSString stringWithFormat:@"%d", percent];
    NSString *currentlyDownloading = [NSString stringWithFormat:@"%d", count];
    NSDictionary *payload = @{@"percent": progress, @"currentlyDownloading": currentlyDownloading};
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        [Jockey send:@"ios-download-progress" withPayload:payload toWebView:self.viewController.webView];
    });
}

- (void) fileReady:(FileDownloader *)sender isThreaded:(bool)threaded
{
    NSDictionary *payload = @{@"key": sender.key};
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [Jockey send:@"ios-file-ready" withPayload:payload toWebView:self.viewController.webView];
    });

    NSLog(@"File is ready: %@ (%@)", sender.fileName, [[sender getDestinationUrl] absoluteString]);
}

- (NSURL *) getFile:(NSString *)key
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSArray *documentURLS = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    NSURL *documentsDirectory = [documentURLS firstObject];
    
    NSArray *keys = [NSArray arrayWithObject:NSURLIsDirectoryKey];
    NSDirectoryEnumerator *enumerator = [fileManager
                                         enumeratorAtURL:documentsDirectory
                                         includingPropertiesForKeys:keys
                                         options:0
                                         errorHandler:^(NSURL *url, NSError *error) {
                                             return YES;
                                         }];
    
    for (NSURL *url in enumerator) {
        NSString *fileName = [[[url absoluteString] lastPathComponent] stringByDeletingPathExtension];
        
        if ([fileName isEqualToString:key]) {
            return url;
        }
    }
    
    return nil;
}

- (void) triggerFileAction:(NSString *)key
{
    NSURL *file = [self getFile:key];
    
    if (file != nil) {
        NSString* ext = [file pathExtension];
        if ([ext isEqualToString:@"mp4"]) {
            [self.viewController playVideo:file];
        }
    }
}

@end