//
//  VideoHandler.h
//  OfflineExample
//
//  Created by Lien Van Nuffel on 21/09/16.
//  Copyright © 2016 USW. All rights reserved.
//

#import "FilesProvider.h"

@class FileDownloader;

@protocol DownloadDelegate
@optional

- (void) progressChanged:(FileDownloader *)sender;
- (void) fileReady:(FileDownloader *)sender isThreaded:(bool)threaded;

@end