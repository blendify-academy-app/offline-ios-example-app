//
//  FilesProvider.h
//  OfflineExample
//
//  Created by Lien Van Nuffel on 21/09/16.
//  Copyright © 2016 USW. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FileDownloader.h"
#import "DownloadDelegate.h"
#import "Jockey.h"
#import "ViewController.h"

@protocol DownloadDelegate;

@class ViewController;

@interface FilesProvider:NSObject <DownloadDelegate>

@property (strong, nonatomic) NSMutableArray *downloads;
@property (strong, nonatomic) ViewController* viewController;

- (id) init:(ViewController *)viewController;

- (void) downloadFileInQueue:(NSString *)url fileKey:(NSString *)key fileExt:(NSString *)ext;

- (void) triggerFileAction:(NSString *)key;

- (void) bindEvents;

@end