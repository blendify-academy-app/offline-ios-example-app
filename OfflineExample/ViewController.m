//
//  ViewController.m
//  OfflineExample
//
//  Created by Lien Van Nuffel on 29/08/16.
//  Copyright © 2016 USW. All rights reserved.
//

#import "ViewController.h"

// #define ENDPOINT @"http://uwsoftware.be/someTest/"

#define ENDPOINT @"http://pilot-2215.dev.imindsx.org/offline/2"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    FilesProvider *fp = [[FilesProvider alloc] init:self];
    [fp bindEvents];
    
    [_webView setDelegate:self];

    [self setTestEvents];
    [self startWebview];
}

- (void)setTestEvents
{
    [Jockey on:@"return_progress" perform:^(NSDictionary *payload) {
        NSLog(@"Progress: %@", payload[@"progress"]);
        NSLog(@"Ready: %@", payload[@"ready"]);
    }];
}

- (IBAction)testButton:(id)sender
{
    NSDictionary *payload = @{};
    [Jockey send:@"get_progress" withPayload:payload toWebView:_webView];
}

- (void)startWebview
{
    NSString *urlAddress = [NSString stringWithFormat:@"%@", ENDPOINT];
    
#ifdef NO_CACHE
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [[NSURLCache sharedURLCache] setDiskCapacity:0];
    [[NSURLCache sharedURLCache] setMemoryCapacity:0];
    
    NSString *uuid = [[NSUUID UUID] UUIDString];
    urlAddress = [NSString stringWithFormat:@"%@?t=%@", urlAddress, uuid];
#endif
    
    NSLog(@"Starting URL: %@", urlAddress);
    
    NSURL *url = [NSURL URLWithString:urlAddress];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:request];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }

    return [Jockey webView:webView withUrl:[request URL]];
}

- (void) webViewDidStartLoad:(UIWebView *)webView
{
    [webView stringByEvaluatingJavaScriptFromString:@"window.iosWebview = 1"];
}

- (void)playVideo:(NSURL *)url
{
    VideoPlayerController *playerController = [[VideoPlayerController alloc] initWithURL:url nibName:@"VideoPlayerController" nibBundle:nil];
    [self presentViewController:playerController animated:YES completion:nil];
}


@end
