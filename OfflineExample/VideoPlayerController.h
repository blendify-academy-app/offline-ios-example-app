//
//  VideoPlayerController.h
//  OfflineExample
//
//  Created by Lien Van Nuffel on 21/09/16.
//  Copyright © 2016 USW. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

@interface VideoPlayerController : AVPlayerViewController

- (id) initWithURL:(NSURL *)videoUrl nibName:(NSString *)name nibBundle:(NSBundle *)bundle;

@end