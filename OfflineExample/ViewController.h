//
//  ViewController.h
//  OfflineExample
//
//  Created by Lien Van Nuffel on 29/08/16.
//  Copyright © 2016 USW. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Downloader/FilesProvider.h"
#import "Jockey.h"
#import "VideoPlayerController.h"

@interface ViewController : UIViewController <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;

- (void)startWebview;

- (void)playVideo:(NSURL *)url;

@end

